class HinhTron
    def initialize(r)
        @bankinh = r
        @pi = 3.14
    end
    def chuvi()
        print "Chu vi hinh tron la: "
        puts 2*@pi*@bankinh
    end
    def dientich()
        print "Dien tich hinh tron la: "
        puts @pi*@bankinh*@bankinh
    end
end

print "Nhap vao ban kinh hinh tron: "
r = gets
h1 = HinhTron.new(r.to_i)
h1.chuvi
h1.dientich