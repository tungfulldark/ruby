mangso = Array.new

# NHap mang
print "Nhap vao so phan tu mang: "
n = gets.chomp
for i in 0..n.to_i-1 do
    print "Nhap vao phan tu thu #{i+1}: "
    giatri = gets.chomp
    mangso.insert(i,giatri)
end
puts "Mang vua nhap la: #{mangso}"

# Tinh gia tri trung binh
sum = 0
dem = 0
for i in 0..mangso.size-1 do
    if mangso[i].to_i%2==0
        sum += mangso[i].to_i
        dem += 1
    end
end
puts "Gia tri trung binh so chan trong mang la: #{sum/dem}"